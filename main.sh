#!/bin/bash


if [ ! -f serverList.txt ]; then
	echo -e "\033[0;31m serverList.txt does not exist in this working directory \033[0m"
	exit 1
fi

echo -e "\033[0;35m This script is used to enable or disable ssh root login \033[0m"

# Get username from usernaem and password
read -p "Please enter your username: " USER
read -p "Please Enter Your Password: " -s PASS

# select section which defines what we want to do "Enable" or "Disable" root login
echo
echo -e "\033[0;32m What do you whant to do:  \033[0m"
select action in 'Enable' 'Disable'
do
	if [ $REPLY == '1' ]; then
		while read SERVER
		do
        		echo $SERVER
        		ssh -t -n $USER@$SERVER "echo "$PASS" |sudo -S sed -i -e 's/^PermitRootLogin no$/PermitRootLogin yes/' /etc/ssh/sshd_config;sudo systemctl restart sshd;sudo systemctl status sshd"
			if [ "$?" = "0" ]; then
				echo "Server name or IP: $SERVER / Date: $(date) : root login has been enabled successfuly">>./status.log
			else
				echo "Server name or IP: $SERVER / Date: $(date) : root login has NOT been enabled successfuly">>./status.log
			fi


		done<serverList.txt
		exit 0

	elif [ $REPLY == '2' ]; then
	       while read SERVER
                do
                        echo $SERVER
                        ssh -t -n $USER@$SERVER "echo "$PASS" |sudo -S sed -i -e 's/^PermitRootLogin yes$/PermitRootLogin no/' /etc/ssh/sshd_config;sudo systemctl restart sshd;sudo systemctl status sshd"
                        if [ "$?" = "0" ]; then
                                echo "Server name or IP: $SERVER / Date: $(date) : root login has been disabled successfuly">>./status.log
                        else
                                echo "Server name or IP: $SERVER / Date: $(date) : root login has NOT been disabled successfuly">>./status.log
                        fi


                done<serverList.txt
                exit 0	

	else
		echo -e "\033[0;31m Invalid input  \033[0m"
		exit 1

	fi
done


